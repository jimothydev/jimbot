import os
import random
import re
from discord.ext import commands
from discord import Embed
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

bot = commands.Bot(command_prefix='!')

@bot.command(name='r', help="Simulates rolling dice.")
async def roll_dice(ctx, dice_txt: str):
    x = re.split("^(\d*)[dD](\d+)$", dice_txt)
    if (len(x) != 4):
        await ctx.send(ctx.author.mention + ' Invalid input value, should be in form "d12" or  "3d12".')
    else:
        if (x[1] == ''):
            x[1] = 1;

        dice = [ #CREATE RANDOM DICE
            str(random.choice(range(1, int(x[2]) + 1)))
            for _ in range(int(x[1]))
        ]

        for index, value in enumerate(dice, 1): #NUMBER THEM
            dice[index-1] = "{}. **{}**".format(index, value);

        batch_size = 50;

        for i in range(0, len(dice), batch_size):
            batch = dice[i:i+batch_size]

            embed = Embed(title="Results for rolling "  + x[1] + " d" + x[2], type='rich', description='\n'.join(batch));
            await ctx.send(ctx.author.mention, embed=embed)

            
@bot.command(name='m8', help="Simulates a Magic 8 ball.")
async def magic_eight(ctx):
    responses = [
        "It is certain.",
        "It is decidedly so.",
        "Without a doubt.",
        "Yes definitely.",
        "You may rely on it.",
        "As I see it, yes.",
        "Most likely.",
        "Outlook good.",
        "Yes.",
        "Signs point to yes.",
        "Reply hazy, try again.",
        "Ask again later.",
        "Better not tell you now.",
        "Cannot predict now.",
        "Concentrate and ask again.",
        "Don't count on it.",
        "My reply is no.",
        "My sources say no.",
        "Outlook not so good.",
        "Very doubtful."
    ]

    await ctx.send(ctx.author.mention + " " + random.choice(responses))
        

@bot.event
async def on_ready():
    print(f'{bot.user.name} has connected to Discord!')

bot.run(TOKEN)